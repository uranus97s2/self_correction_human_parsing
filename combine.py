import os
import cv2
import numpy as np

path1 = '/media/duongpd/Data/module1/test_dataset/test/body_pose_original'
path2 = '/media/duongpd/Data/module1/test_dataset/test/500_STAFF/labels_8_classes'
save = '/media/duongpd/Data/module1/test_dataset/test/500_STAFF/8_body_parts_segments_test'
l = os.listdir(path1)
for i in l:
    im = cv2.imread(os.path.join(path1, i))
    h, w, c = im.shape
    if h > 5000 and w > 4000:
        im = cv2.resize(im, (int(w * 0.8), int(h * 0.8)))
        h, w, _ = im.shape

    if h > 4000 and w > 3000:
        im = cv2.resize(im, (int(w * 0.8), int(h * 0.8)))
        h, w, _ = im.shape

    seg = cv2.imread(os.path.join(path2, i))
    hair = cv2.inRange(seg, (0, 0, 255), (0, 0, 255))
    hair = cv2.cvtColor(hair, cv2.COLOR_GRAY2BGR)

    face = cv2.inRange(seg, (255, 0, 0), (255, 0, 0))
    face = cv2.cvtColor(face, cv2.COLOR_GRAY2BGR)

    im[np.where((im == [200, 35, 135]).all(axis=2))] = (255, 0, 0)

    out = im | hair
    out[np.where((out == [255, 255, 255]).all(axis=2))] = (0, 0, 255)

    out = out | face
    out[np.where((out == [255, 255, 255]).all(axis=2))] = (255, 0, 0)

    trunk = cv2.inRange(im, (170, 35, 170), (170, 35, 170))
    trunk = cv2.cvtColor(trunk, cv2.COLOR_GRAY2BGR)

    out2 = face & trunk
    out[np.where((out == [200, 35, 135]).all(axis=2))] = (255, 0, 0)

    set_x = out | out2
    set_x[np.where((out2 == [255, 255, 255]).all(axis=2))] = (170, 35, 170)

    left_arm = cv2.inRange(im, (255, 215, 50), (255, 215, 50))
    left_arm = cv2.cvtColor(left_arm, cv2.COLOR_GRAY2BGR)
    out3 = face & left_arm
    set_x = set_x | out3
    set_x[np.where((set_x == [255, 255, 255]).all(axis=2))] = (255, 215, 50)

    right_arm = cv2.inRange(im, (85, 155, 45), (85, 155, 45))
    right_arm = cv2.cvtColor(right_arm, cv2.COLOR_GRAY2BGR)
    out4 = face & right_arm
    set_x = set_x | out4
    set_x[np.where((set_x == [255, 255, 255]).all(axis=2))] = (85, 155, 45)

    cv2.imwrite(os.path.join(save, i.replace('jpg', 'png')), set_x)
