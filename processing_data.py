import os
import cv2.cv2 as cv2
import numpy as np

colors = {
    'left_arm': (255, 215, 50),
    'right_arm': (85, 155, 45),
    'trunk': (170, 35, 170),
    'left_leg': (35, 70, 170),
    'right_leg': (100, 175, 165),
    'head': (200, 35, 135),
    'hair': (0, 0, 255),
    'face': (255, 0, 0)
}


#
# path_body_parts = '/media/duongpd/Data/module1/test_dataset/test/500_STAFF/8_body_parts_segments'
# for i in os.listdir(path_body_parts):
#     seg = cv2.imread(os.path.join(path_body_parts, i), -1)
#     seg[np.where((seg == colors['head']).all(axis=2))] = colors['face']
#     cv2.imshow("face", seg)
#     # cv2.imshow("head", head)
#     cv2.waitKey(0)

# path_images = "/media/duongpd/Data/module1/test_dataset/test/images"
# path_seg = "/media/duongpd/Data/module1/test_dataset/test/500_STAFF/8_body_parts_segments"
#
# path_save_images_flip = '/media/duongpd/Data/module1/test_dataset/test/image_flip'
# path_save_seg_flip = '/media/duongpd/Data/module1/test_dataset/test/seg_flip'
# for item in os.listdir(path_seg):
#
#     im = cv2.imread(os.path.join(path_images, item.replace("png", "jpg")))
#     seg = cv2.imread(os.path.join(path_seg, item))
#     im2 = cv2.flip(im, 1)
#     seg2 = cv2.flip(seg, 1)
#     seg2[np.where((seg2 == colors['left_arm']).all(axis=2))] = (10, 10, 10)
#     seg2[np.where((seg2 == colors['right_arm']).all(axis=2))] = colors['left_arm']
#     seg2[np.where((seg2 == (10, 10, 10)).all(axis=2))] = colors['right_arm']
#
#     seg2[np.where((seg2 == colors['left_leg']).all(axis=2))] = (20, 20, 20)
#     seg2[np.where((seg2 == colors['right_leg']).all(axis=2))] = colors['left_leg']
#     seg2[np.where((seg2 == (20, 20, 20)).all(axis=2))] = colors['right_leg']
#     # if item.split("_")[-1] == "flip.png":
#     #     os.remove(os.path.join(path_images, item.replace("_flip.png", "_flip.jpg")))
#     #     os.remove(os.path.join(path_seg, item))
#     cv2.imwrite(os.path.join(path_images, item.replace(".png", "_flip.jpg")), im2)
#     cv2.imwrite(os.path.join(path_seg, item.replace(".png", "_flip.png")), seg2)
#

# path = '/media/duongpd/Data/module1/test_dataset/test/500_STAFF/background'
# l = os.listdir(path)
# for idx, item in enumerate(l):
#     os.rename(os.path.join(path, item),
#               os.path.join(path, 'bg_{}.jpg'.format(idx + 1)))


def resize_and_pad(im, desired_size):
    # old_size is in (height, width) format
    old_size = im.shape[:2]
    ratio = float(desired_size) / max(old_size)

    # new_size should be in (width, height) format
    new_size = tuple([int(x * ratio) for x in old_size])
    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)
    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)

    return new_im, [top, bottom, left, right]


path = '/media/duongpd/Data/module1/test_dataset/test/500_STAFF/background'
path_img = '/media/duongpd/Data/module1/test_dataset/test/images'
path_seg = '/media/duongpd/Data/module1/test_dataset/test/500_STAFF/8_body_parts_segments'

path_save_img = '/media/duongpd/Data/module1/test_dataset/test/500_STAFF/image_train'
path_save_seg = '/media/duongpd/Data/module1/test_dataset/test/500_STAFF/seg_train'
for i in os.listdir(path_img):
    try:
        img = cv2.imread(os.path.join(path_img, i))
        seg = cv2.imread(os.path.join(path_seg, i.replace('.jpg', '.png')))
        x, y, w, h = cv2.boundingRect(~cv2.inRange(seg, (0, 0, 0), (0, 0, 0)))
        mask = ~cv2.inRange(seg, (0, 0, 0), (0, 0, 0))
        sub_img = cv2.bitwise_and(img, img, mask=mask)
        box_img = sub_img[y: y + h, x: x + w]
        seg_img = seg[y: y + h, x: x + w]
        for x in os.listdir(path):
            bg = cv2.imread(os.path.join(path, x))
            bg = cv2.resize(bg, (512, 512))
            h1, w1 = bg.shape[:2]
            h2, w2 = box_img.shape[:2]
            out_img, _ = resize_and_pad(box_img, 512)
            out_seg, _ = resize_and_pad(seg_img, 512)
            fg = cv2.bitwise_and(bg, bg, mask=cv2.inRange(out_seg, (0, 0, 0), (0, 0, 0)))
            full_img = cv2.add(fg, out_img)
            cv2.imwrite(os.path.join(path_save_img, '{}_{}.jpg'.format(i.split('.')[0], x.split('.')[0])), full_img)
            cv2.imwrite(os.path.join(path_save_seg, '{}_{}.png'.format(i.split('.')[0], x.split('.')[0])), out_seg)
            # cv2.imshow('Im', full_img)
            # cv2.imshow('seg', out_seg)
            # cv2.waitKey(0)
    except Exception as e:
        print(e)