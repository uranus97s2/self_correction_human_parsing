import pandas as pd
import argparse

import tensorflow as tf

from utils import shuffle
from config import cfg
from losses import Cross_Entropy, dice_loss, sigmoid_cross_entropy_balanced, consistent_loss
from preprocessing.dataset_train import pre_processing
from self_correction_human_parsing import SHCP

dataset_settings = {
    'smex': {
        'input_size': [512, 512],
        'num_classes': 8,
        'label': ["background", "hair", "face", "trunk", "left_arm", "right_arm", "left_leg", "right_leg"]
    }
}


def get_arguments():
    parser = argparse.ArgumentParser(description="Self Correction for Human Parsing with module 1")
    parser.add_argument("--dataset", type=str, default='smex', choices=['smex'])
    parser.add_argument("--restore-weight", type=str, default='model/*',
                        help="restore pretrained model parameters.")
    parser.add_argument("--input", type=str, default='',
                        help="path of input image folder.")
    parser.add_argument("--output", type=str, default='',
                        help="path of output image folder.")
    return parser.parse_args()


def main():
    args = get_arguments()
    num_classes = dataset_settings[args.dataset]['num_classes']
    input_size = dataset_settings[args.dataset]['input_size']

    model = SHCP(num_classes=num_classes)

    labels = tf.placeholder(dtype=tf.float32, shape=(None, *input_size, num_classes))
    edge_label = tf.placeholder(dtype=tf.float32, shape=(None, *input_size, 1))
    inputs = tf.placeholder(dtype=tf.float32, shape=(None, *input_size, 3))

    lambda_1, lambda_2, lambda_3 = 1., 1., .1
    prediction, edge_out = model(inputs)

    # Get edge by tf
    edge_label_tf = tf.image.sobel_edges(edge_label)
    edge_label_tf = tf.squeeze(edge_label_tf, axis=-2)

    # Get feat_edge_maps
    edge_maps = prediction * 255.
    edge_maps = tf.cast(tf.argmax(edge_maps, axis=-1), dtype=tf.float32) / 255.
    edge_maps = tf.expand_dims(edge_maps, axis=-1)
    edge_maps = tf.image.sobel_edges(edge_maps)
    edge_maps = tf.squeeze(edge_maps, axis=-2)

    # Loss Edge
    loss_edge = sigmoid_cross_entropy_balanced(logits=tf.nn.sigmoid(edge_out),
                                               label=tf.nn.sigmoid(edge_label_tf))

    # Loss Parsing
    loss_parsing = Cross_Entropy(y_true=labels, y_pred=prediction) + dice_loss(y_true=labels, y_pred=prediction)

    # Loss Consistent
    loss_consistent = consistent_loss(logits=tf.nn.sigmoid(edge_out),
                                      feats_edge_map=tf.nn.sigmoid(edge_maps))

    total_loss = lambda_1 * loss_edge + lambda_2 * loss_parsing + lambda_3 * loss_consistent

    # Optimizer
    train_op = tf.train.AdamOptimizer(cfg.lr).minimize(total_loss)

    # IOU
    mean_iou_value, mean_iou_update_op = tf.metrics.mean_iou(labels=labels,
                                                             predictions=prediction,
                                                             num_classes=num_classes)
    # Accuracy
    acc_value, acc_update_op = tf.metrics.accuracy(labels=labels,
                                                   predictions=prediction)

    lst_labels = []
    lst_preds = []
    for i in range(1, num_classes):
        channel_lb = labels[:, :, :, i:(i + 1)] * (20 * (i + 1))

        channel_pred = prediction[:, :, :, i:(i + 1)] * (20 * (i + 1))
        lst_labels.append(channel_lb)
        lst_preds.append(channel_pred)

    sum_label = lst_labels[0]
    sum_pred = lst_preds[0]
    for i in range(1, len(lst_labels)):
        sum_label = tf.add(sum_label, lst_labels[i])
        sum_pred = tf.add(sum_pred, lst_preds[i])

    # Summary
    tf.summary.scalar("Training - IOU", mean_iou_value, family="metrics")
    tf.summary.scalar("Training - Accuracy", acc_value, family="metrics")
    tf.summary.scalar("Training - Total - loss", total_loss, family='Loss')
    tf.summary.scalar("Training - Edge - Loss", loss_edge, family='Loss')
    tf.summary.scalar("Training - Parsing - Loss", loss_parsing, family='Loss')
    tf.summary.scalar("Training - Consistent - Loss", loss_consistent, family='Loss')

    with tf.name_scope("grouth_true"):
        tf.summary.image("label_tensor", sum_label)

    with tf.name_scope('Output'):
        tf.summary.image("prediction", sum_pred)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())
    saver = tf.train.Saver()
    saver.restore(sess, cfg.path_save_model)
    summary_op = tf.summary.merge_all()
    summary_writer = tf.summary.FileWriter(cfg.log_dir, sess.graph)
    data = pd.read_csv(cfg.path_train)

    # Training
    for epoch in range(cfg.epoch_size):
        data = shuffle(data)
        for i in range(0, len(data), cfg.train_batch_size):
            itr = i // cfg.train_batch_size
            batchs = data.iloc[i: i + cfg.train_batch_size]
            try:
                im, lb, edge_lb = pre_processing(batchs)

                # Update weights
                loss_val, summary_str, opt_d, _, _ = sess.run([total_loss, summary_op,
                                                               train_op,
                                                               mean_iou_update_op,
                                                               acc_update_op], feed_dict={
                    inputs: im,
                    labels: lb,
                    edge_label: edge_lb
                })
                print("epoch - {} | iter - {}    |   d-loss - {}".format(epoch, itr, loss_val))
                summary_writer.add_summary(summary_str, itr)


            except Exception as e:
                print(e)

        if epoch % 2 == 0:
            saver.save(sess, cfg.path_save_model)
            print("Successful !!!")


if __name__ == '__main__':
    main()
