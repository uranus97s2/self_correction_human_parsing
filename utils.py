
def update_weights(w1, w2, m = 0):

    return (m/(m+1))*w1 + (1/(m+1))*w2


def update_labels(y, _y, m = 0):

    return (m/(m+1))*y + (1/(m+1))*_y


import itertools


def shuffle(df, n_split=5):
    df = df.copy()
    df = df.sample(frac=1).reset_index(drop=True)
    df['id'] = get_idx(len(df), n_split)
    return df


def get_idx(len_data, n_split=10):
    loop = len_data // n_split

    lst_idx = []
    for i in range(n_split):
        lst_idx += [*itertools.repeat(i, loop)]
    lst_idx = [*lst_idx, *itertools.repeat(n_split - 1, len_data % n_split)]
    return lst_idx


def filter_data_by_id(dataFrame, id_value):
    return dataFrame[dataFrame['id'] == id_value]