import argparse

import cv2.cv2 as cv2
import numpy as np
import tensorflow as tf

from PIL import Image
from config import cfg
from preprocessing.dataset import SMFDataset
from preprocessing.dataset_train import transform


dataset_settings = {
    'smex': {
        'input_size': [512, 512],
        'num_classes': 8,
        'label': ["background", "hair", "face", "trunk", "left_arm", "right_arm", "left_leg", "right_leg"]
    }
}


def get_arguments():
    parser = argparse.ArgumentParser(description="Self Correction for Human Parsing with module 1")
    parser.add_argument("--dataset", type=str, default='smex', choices=['smex'])
    parser.add_argument("--restore_weight", type=str, default='model/schp.pb',
                        help="check model parameters.")
    parser.add_argument("--input", type=str, default='test/input',
                        help="path of input image folder.")
    parser.add_argument("--output", type=str, default='test/output',
                        help="path of output image folder.")
    return parser.parse_args()


def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we import the graph_def into a new Graph and returns it
    with tf.Graph().as_default() as graph:
        # The name var will prefix every op/nodes in your graph
        # Since we load everything in a new graph, this is not needed

        # fix nodes
        for node in graph_def.node:
            if node.op == 'RefSwitch':
                node.op = 'Switch'
                for index in range(len(node.input)):
                    node.input[index] = node.input[index] + '/read'
            elif node.op == 'AssignSub':
                node.op = 'Sub'
                if 'use_locking' in node.attr: del node.attr['use_locking']
        tf.import_graph_def(graph_def, name="")

    return graph


if __name__ == '__main__':
    import os

    args = get_arguments()
    graph = load_graph(args.restore_weight)

    # for op in graph.get_operations():
    #      print(op.name)

    inputs = graph.get_tensor_by_name("input_tensor:0")
    schp_outputs = graph.get_tensor_by_name("schp_output:0")
    # initial
    palette = [0, 0, 0, 128, 0, 0, 0, 128, 0, 128, 128, 0, 0, 0, 128, 128, 0, 128, 0, 128, 128, 128, 128, 128, 64, 0, 0]
    smf = SMFDataset(input_size=cfg.data_shape)

    with tf.Session(graph=graph) as sess:
        for i in os.listdir(args.input):
            img = cv2.imread(os.path.join(args.input, i))
            img, meta = smf.process(img, transform=transform)
            out = sess.run(schp_outputs, feed_dict={
                inputs: [img],
            })
            out = out[0]
            out = smf.restore_data_by_meta(out, meta)
            parsing_result = np.argmax(out, axis=2)
            output_img = Image.fromarray(np.asarray(parsing_result, dtype=np.uint8))
            output_img.putpalette(palette)
            output_img.save(os.path.join(args.output, i.replace(i.split('.')[-1], 'png')))
