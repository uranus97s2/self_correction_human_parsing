import argparse
import os
import tensorflow as tf

from config import cfg
from self_correction_human_parsing import SHCP


dataset_settings = {
    'smex': {
        'input_size': [512, 512],
        'num_classes': 8,
        'label': []
    }
}


def get_arguments():
    parser = argparse.ArgumentParser(description="Self Correction for Human Parsing with module 1")
    parser.add_argument("--dataset", type=str, default='smex', choices=['smex'])
    parser.add_argument("--save_weight", type=str, default='model/schp.pb',
                        help="frezee model parameters.")
    parser.add_argument("--input", type=str, default='',
                        help="path of input image folder.")
    parser.add_argument("--output", type=str, default='',
                        help="path of output image folder.")
    return parser.parse_args()


def main():
    args = get_arguments()
    num_classes = dataset_settings[args.dataset]['num_classes']
    input_size = dataset_settings[args.dataset]['input_size']

    model = SHCP(num_classes=num_classes)

    inputs = tf.placeholder(dtype=tf.float32, shape=(None, *input_size, 3), name='input_tensor')

    prediction, edge_out = model(inputs)
    prediction = tf.identity(prediction, "schp_output")

    sess = tf.Session()

    saver = tf.train.Saver()
    sess.run(tf.global_variables_initializer())
    saver.restore(sess, cfg.path_save_model)

    # for fixing the bug of batch norm
    gd = sess.graph.as_graph_def()
    for node in gd.node:
        if node.op == 'RefSwitch':
            node.op = 'Switch'
            for index in range(len(node.input)):
                if 'moving_' in node.input[index]:
                    node.input[index] = node.input[index] + '/read'
        elif node.op == 'AssignSub':
            node.op = 'Sub'
            if 'use_locking' in node.attr: del node.attr['use_locking']
        elif node.op == 'AssignAdd':
            node.op = 'Add'
            if 'use_locking' in node.attr: del node.attr['use_locking']

    # EXPORT .......................
    output_graph_def = tf.compat.v1.graph_util.convert_variables_to_constants(
        sess,  # The session is used to retrieve the weights
        gd,  # The graph_def is used to retrieve the nodes
        ['input_tensor', 'schp_output']  # The output node names are used to select the usefull nodes
    )
    path_save_weight = args.save_weight.split('/')
    root, name_model = '/'.join(path_save_weight[:-1]), path_save_weight[-1]
    if not os.path.exists(root):
        os.mkdir(root)

    with tf.gfile.GFile(args.save_weight, "wb") as f:
        f.write(output_graph_def.SerializeToString())
        print("%d ops in the final graph." % len(output_graph_def.node))


if __name__ == '__main__':
    main()
