import tensorflow as tf

from networks.bottleneck import Bottleneck
from networks.resnet101 import ResNet


class SHCP:
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.resnet_bottleneck = ResNet(Bottleneck, [3, 4, 23, 3], self.num_classes)

    def __call__(self, feats):
        net, edge = self.resnet_bottleneck.forward(feats)
        net = tf.keras.layers.UpSampling2D(size=(4, 4), data_format=None, interpolation='bilinear')(net)
        edge =tf.keras.layers.UpSampling2D(size=(4, 4), data_format=None, interpolation='bilinear')(edge)
        return tf.nn.sigmoid(net), edge


if __name__ == '__main__':
    x = tf.placeholder(dtype=tf.float32, shape=(None, 224, 224, 3))
    schp = SHCP(num_classes=18)
    inference, edge = schp(x)
    print(inference)