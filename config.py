from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os


class Config:
    username = 'default'

    cur_dir = os.path.dirname(os.path.abspath(__file__))
    this_dir_name = cur_dir.split('/')[-1]
    root_dir = os.path.join(cur_dir, '..', '..')

    proj_name = this_dir_name

    seed = 2019
    lr = 2e-4
    lr_gammxa = 0.5
    train_batch_size = 2
    epoch_size = 1000
    optimizer = 'adam'

    gpu_ids = '0'
    nr_gpus = 1
    continue_train = False

    bn_train = True
    data_shape = (512, 512) #height, width

    # from utility import data_batch
    log_dir = '/media/duongpd/Data/module1/test_dataset/test/logdir'
    path_images = "{}/images".format(cur_dir)
    path_labels = "{}/body_pose_original".format(cur_dir)

    # TRAINING ..............................
    path_train = '{}/preprocessing/dataset_train.csv'.format(cur_dir)
    path_test = 'test_dataset.csv'
    path_save_model = '/media/duongpd/Data/module1/test_dataset/test/model/schp_model.ckpt'


cfg = Config()

