import argparse
import os
import tensorflow as tf

from config import cfg
from self_correction_human_parsing import SHCP


dataset_settings = {
    'smex': {
        'input_size': [512, 512],
        'num_classes': 8,
        'label': []
    }
}


def get_arguments():
    parser = argparse.ArgumentParser(description="Self Correction for Human Parsing with module 1")
    parser.add_argument("--dataset", type=str, default='smex', choices=['smex'])
    parser.add_argument("--save_weight", type=str, default='model/schp.pb',
                        help="frezee model parameters.")
    parser.add_argument("--input", type=str, default='',
                        help="path of input image folder.")
    parser.add_argument("--output", type=str, default='',
                        help="path of output image folder.")
    return parser.parse_args()


def main():
    args = get_arguments()
    num_classes = dataset_settings[args.dataset]['num_classes']
    input_size = dataset_settings[args.dataset]['input_size']

    model = SHCP(num_classes=num_classes)

    inputs = tf.placeholder(dtype=tf.float32, shape=(None, *input_size, 3), name='input_tensor')

    prediction, edge_out = model(inputs)
    prediction = tf.identity(prediction, "schp_output")

    sess = tf.Session()

    saver = tf.train.Saver()
    sess.run(tf.global_variables_initializer())
    saver.restore(sess, cfg.path_save_model)

    model_version = "1"
    export_path_base = "../export_dir"
    export_path = os.path.join(
        tf.compat.as_bytes(export_path_base),
        tf.compat.as_bytes(str(model_version)))
    print('Exporting trained model to', export_path)
    builder = tf.saved_model.builder.SavedModelBuilder(export_path)

    # Creates the TensorInfo protobuf objects that encapsulates the input / output tensors
    tensor_info_input = tf.saved_model.utils.build_tensor_info(inputs)

    # output tensor info
    tensor_info_output = tf.saved_model.utils.build_tensor_info(prediction)
    prediction_signature = (
        tf.saved_model.signature_def_utils.build_signature_def(
            inputs={'image': tensor_info_input},
            outputs={'output_body_parts': tensor_info_output},
            method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME))

    builder.add_meta_graph_and_variables(
        sess, [tf.saved_model.tag_constants.SERVING],
        signature_def_map={
            'predict_images':
                prediction_signature,
        })
    builder.save(as_text=True)
    print("Done exporting!")


if __name__ == '__main__':
    main()
