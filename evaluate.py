import argparse

import tensorflow as tf

from self_correction_human_parsing import SHCP

dataset_settings = {
    'smex': {
        'input_size': [512, 512],
        'num_classes': 8,
        'label': []
    }
}


def get_arguments():
    parser = argparse.ArgumentParser(description="Self Correction for Human Parsing with module 1")
    parser.add_argument("--dataset", type=str, default='smex', choices=['smex'])
    parser.add_argument("--restore-weight", type=str, default='model/*',
                        help="restore pretrained model parameters.")
    parser.add_argument("--input", type=str, default='',
                        help="path of input image folder.")
    parser.add_argument("--output", type=str, default='',
                        help="path of output image folder.")
    return parser.parse_args()


def main():
    args = get_arguments()
    num_classes = dataset_settings[args.dataset]['num_classes']
    input_size = dataset_settings[args.dataset]['input_size']
    label = dataset_settings[args.dataset]['label']
    model = SHCP(num_classes=18)


if __name__ == '__main__':
    main()