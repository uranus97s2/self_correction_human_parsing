import tensorflow as tf

from networks.utils import get_fil, batch_norm


class Bottleneck:
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, dilation=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.inplanes = inplanes
        self.planes = planes
        self.stride = stride
        self.dilation = dilation
        self.downsample = downsample

    def forward(self, x):
        residual = x

        # conv1
        out = tf.nn.conv2d(x, get_fil([1, 1, self.inplanes, self.planes]), [1, 1, 1, 1], padding='SAME')
        out = batch_norm(out)
        out = tf.nn.relu(out)

        # conv2
        out = tf.nn.conv2d(out, get_fil([3, 3, self.planes, self.planes]), [1, self.stride, self.stride, 1],
                           padding='SAME', dilations=[1, self.dilation, self.dilation, 1])
        out = batch_norm(out)
        out = tf.nn.relu(out)

        # conv3
        out = tf.nn.conv2d(out, get_fil([1, 1, self.planes, self.planes * 4]), [1, 1, 1, 1], padding='SAME')
        out = batch_norm(out)

        if self.downsample is not None:
            residual = self.downsample(x)
        out = out + residual
        out = tf.nn.relu(out)

        return out
