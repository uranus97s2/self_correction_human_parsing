import tensorflow as tf
from networks.bottleneck import Bottleneck
from networks.decode_module import DecoderModule
from networks.egde import EdgeModule
from networks.in_place_abn_sync import InPlaceABNSync
from networks.pspmodule import PSPModule
from networks.utils import batch_norm, get_fil


def resnet_conv2d(x, in_planes, out_planes, kernel, stride=1):
    net = tf.nn.conv2d(x, get_fil([kernel, kernel, in_planes, out_planes]), [1, stride, stride, 1], padding='SAME')
    net = tf.layers.batch_normalization(net, training=True)
    net = tf.nn.relu(net)
    return net


class Fushion:
    def __init__(self, num_clasess):
        self.num_clasess = num_clasess
        self.inplace_abn_sync = InPlaceABNSync()

    def forward(self, x):
        net = tf.nn.conv2d(x, get_fil([1, 1, 1024, 256]), [1, 1, 1, 1], padding='VALID')
        net = self.inplace_abn_sync.forward(net)
        net = tf.nn.dropout(net, 0.1)
        net = tf.nn.conv2d(net, get_fil([1, 1, 256, self.num_clasess]), [1, 1, 1, 1], padding='VALID')
        return net


class ResNet:
    def __init__(self, block, layers, num_classes):
        self.inplanes = 128
        self.block = block
        self.layers = layers
        self.num_clasess = num_classes
        self.inplace_abn_sync = InPlaceABNSync()
        self.context_encoding = PSPModule()
        self.edge = EdgeModule()
        self.decoder = DecoderModule(self.num_clasess)
        self.fushion = Fushion(self.num_clasess)

    def forward(self, x):
        net = resnet_conv2d(x, in_planes=3, out_planes=64, kernel=3, stride=2)
        net = resnet_conv2d(net, in_planes=64, out_planes=64, kernel=3, stride=1)
        net = resnet_conv2d(net, in_planes=64, out_planes=128, kernel=3, stride=1)

        net1 = tf.layers.max_pooling2d(net, (3, 3), strides=(2, 2), padding='SAME')
        net2 = self._make_layer(net1, self.block, 64, self.layers[0], stride=1)
        net3 = self._make_layer(net2, self.block, 128, self.layers[1], stride=2)
        net4 = self._make_layer(net3, self.block, 256, self.layers[2], stride=2)
        net5 = self._make_layer(net4, self.block, 512, self.layers[3])

        net = self.context_encoding.forward(net5)
        parsing_result, parsing_fea = self.decoder.forward(net, net2)

        # Edge Branch
        edge_result, edge_fea = self.edge.forward(net2, net3, net4)

        # Fushion Branch
        net = tf.concat([parsing_fea, edge_fea], axis=3)
        fusion_result = self.fushion.forward(net)
        return fusion_result, edge_result

    def _make_layer(self, x, block, planes, blocks, stride=1, dilation=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            def downsample(net):
                downsample = tf.nn.conv2d(net, get_fil([1, 1, self.inplanes, planes * block.expansion]),
                                          strides=[1, stride, stride, 1], padding='SAME')
                downsample = batch_norm(downsample)
                return downsample

        layer = block(self.inplanes, planes, stride, dilation=dilation, downsample=downsample).forward(x)
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layer = block(self.inplanes, planes, dilation=dilation).forward(layer)

        return layer


if __name__ == '__main__':
    num_classes = 18
    x = tf.placeholder(dtype=tf.float32, shape=(None, 224, 224, 3))
    net = ResNet(Bottleneck, [3, 4, 23, 3], num_classes)
    inference, edge = net.forward(x)
    print(inference)
    print(edge)