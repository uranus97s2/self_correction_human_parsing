import tensorflow as tf

from networks.in_place_abn_sync import InPlaceABNSync
from networks.utils import get_fil


class PSPModule:
    """
    Reference:
        Zhao, Hengshuang, et al. *"Pyramid scene parsing network."*
    """

    def __init__(self, features=2048, out_features=512, sizes=(1, 2, 3, 6)):
        super(PSPModule, self).__init__()

        self.features = features
        self.out_features = out_features
        self.sizes = sizes
        self.inplace_abn_sync = InPlaceABNSync()

    def _make_stage(self, x, size):
        _, h, w, _ = x.get_shape()
        prior = tf.nn.avg_pool2d(x, ksize=((w + size -1)// size), strides=((w + size -1)// size), padding='VALID')
        conv = tf.nn.conv2d(prior, get_fil([1, 1, self.features, self.out_features]), strides=[1, 1, 1, 1], padding='VALID')
        bn = self.inplace_abn_sync.forward(conv)
        return bn

    def stages(self, x):
        stage = [self._make_stage(x, size) for size in self.sizes]
        return stage

    def bottleneck(self, x):
        conv = tf.nn.conv2d(x, get_fil([3, 3, self.features + len(self.sizes) * self.out_features, self.out_features]),
                            strides=[1, 1, 1, 1],
                            padding='SAME')
        InPlace = self.inplace_abn_sync.forward(conv)
        return InPlace

    def forward(self, feats):
        _, h, w, _ = feats.get_shape()
        priors = [tf.image.resize_images(stage, (h, w)) for stage in self.stages(feats)] + [feats]
        bottle = self.bottleneck(tf.concat(priors, axis=3))

        return bottle


if __name__ == '__main__':
    x = tf.placeholder(dtype=tf.float32, shape=(None, 14, 14, 2048))
    psp = PSPModule().forward(x)
    print(psp)