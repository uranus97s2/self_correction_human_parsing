import tensorflow as tf

from networks.utils import batch_norm


class InPlaceABNSync:
    """
    Serve same as the InplaceABNSync.
    Reference: https://github.com/mapillary/inplace_abn
    """

    def __init__(self):
        self.bn = batch_norm
        self.leaky_relu = tf.nn.leaky_relu

    def forward(self, x):
        x = batch_norm(x)
        x = self.leaky_relu(x)
        return x
