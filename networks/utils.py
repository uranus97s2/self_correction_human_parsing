import tensorflow as tf


def get_fil(kernel_shape):
    return tf.Variable(tf.truncated_normal(kernel_shape, dtype=tf.float32,
                                           stddev=1e-1), name='weights')


def batch_norm(x, is_training=True):
    return tf.layers.batch_normalization(x, epsilon=1e-5, training=is_training)
