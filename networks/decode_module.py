import tensorflow as tf

from networks.in_place_abn_sync import InPlaceABNSync
from networks.utils import get_fil


class DecoderModule:
    """
    Parsing Branch Decoder Module.

    """

    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.inplace_abn_sync = InPlaceABNSync()

    def conv1(self, x):
        net = tf.nn.conv2d(x, get_fil([1, 1, 512, 256]), [1, 1, 1, 1], padding='VALID')
        net = self.inplace_abn_sync.forward(net)
        return net

    def conv2(self, x):
        net = tf.nn.conv2d(x, get_fil([1, 1, 256, 48]), [1, 1, 1, 1], padding='VALID')
        net = self.inplace_abn_sync.forward(net)
        return net

    def conv3(self, x):
        net = tf.nn.conv2d(x, get_fil([1, 1, 304, 256]), [1, 1, 1, 1], padding='VALID')
        net = self.inplace_abn_sync.forward(net)
        net = tf.nn.conv2d(net, get_fil([1, 1, 256, 256]), [1, 1, 1, 1], padding='VALID')
        net = self.inplace_abn_sync.forward(net)
        return net

    def conv4(self, x):
        return tf.nn.conv2d(x, get_fil([1, 1, 256, self.num_classes]), [1, 1, 1, 1], padding='VALID')

    def forward(self, xt, xl):
        _, h, w, _ = xl.get_shape()
        xt = tf.image.resize_images(self.conv1(xt), (h, w), align_corners=True)
        xl = self.conv2(xl)
        x = tf.concat([xt, xl], axis=3)
        x = self.conv3(x)
        seg = self.conv4(x)
        return seg, x


if __name__ == '__main__':
    x = tf.placeholder(dtype=tf.float32, shape=(None, 14, 14, 512))
    x2 = tf.placeholder(dtype=tf.float32, shape=(None, 56, 56, 512))
    parsing_result, parsing_fea= DecoderModule(18).forward(x, x2)
    print(parsing_result)
    print(parsing_fea)