import urllib

import tensorflow as tf

from networks.in_place_abn_sync import InPlaceABNSync
from networks.utils import get_fil


class EdgeModule:
    """
    Edge branch.
    """

    def __init__(self, in_fea=[256, 512, 1024], mid_fea=256, out_fea=2):
        self.in_fea = in_fea
        self.mid_fea = mid_fea
        self.out_fea = out_fea
        self.inplace_abn_sync = InPlaceABNSync()

    def conv_by_idx(self, x, idx):
        net = tf.nn.conv2d(x, get_fil([1, 1, self.in_fea[idx], self.mid_fea]), [1, 1, 1, 1], padding='VALID')
        net = self.inplace_abn_sync.forward(net)
        return net

    def conv(self, x):
        return tf.nn.conv2d(x, get_fil([3, 3, self.mid_fea, self.out_fea]), [1, 1, 1, 1], padding='SAME')

    def conv_x(self, x):
        return tf.nn.conv2d(x, get_fil([1, 1, self.out_fea * 3, self.out_fea]), [1, 1, 1, 1], padding='VALID')

    def forward(self, x1, x2, x3):
        _, h, w, _ = x1.get_shape()

        edge1_fea = self.conv_by_idx(x1, 0)
        edge1 = self.conv(edge1_fea)
        edge2_fea = self.conv_by_idx(x2, 1)
        edge2 = self.conv(edge2_fea)
        edge3_fea = self.conv_by_idx(x3, 2)
        edge3 = self.conv(edge3_fea)

        edge2_fea = tf.image.resize_images(edge2_fea, (h, w), align_corners=True)
        edge3_fea = tf.image.resize_images(edge3_fea, (h, w), align_corners=True)

        edge2 = tf.image.resize_images(edge2, (h, w), align_corners=True)
        edge3 = tf.image.resize_images(edge3, (h, w), align_corners=True)

        edge = tf.concat([edge1, edge2, edge3], axis=3)
        edge_fea = tf.concat([edge1_fea, edge2_fea, edge3_fea], axis=3)
        edge = self.conv_x(edge)

        return edge, edge_fea


if __name__ == '__main__':
    x2 = tf.placeholder(dtype=tf.float32, shape=(None, 56, 56, 256))
    x3 = tf.placeholder(dtype=tf.float32, shape=(None, 28, 28, 512))
    x4 = tf.placeholder(dtype=tf.float32, shape=(None, 14, 14, 1024))
    edge_result, edge_fea = EdgeModule().forward(x2, x3, x4)
    print(edge_result)
    print(edge_fea)