FROM nvidia/cuda:10.0-cudnn7-devel as builder

ARG username
ARG password

RUN apt-get update && apt-get install -y unzip wget tar python3-pip python-pip \
    git virtualenv libglib2.0-0 libsm6 libfontconfig1 libxrender1 openjdk-8-jdk \
    build-essential swig python-wheel python3-wheel libcurl3-dev libcupti-dev \
    libcurl4-openssl-dev

WORKDIR /work/api/

RUN git clone https://${username}:${password}@gitlab-new.bap.jp/duongpd/auto_train.git auto_train

WORKDIR /work/api/auto_train

RUN pip install -r requirements.txt

RUN python download_files.py 1KMrJSlYVXB6xOf4fIsK3y_ap8aND5sZd ./body_pose_original.tar.gz && \
    python download_files.py 1qE_jsb8rSmCDUHV_nhv1hYUMDqc6VKfT ./images.tar.gz && \
    tar -zxvf body_pose_original.tar.gz && tar -zxvf images.tar.gz && \
    rm -f body_pose_original.tar.gz images.tar.gz

RUN export PYTHONPATH=`pwd` && cd preprocessing/ && python dataset_train.py

CMD ["python", "training.py"]
