# Self Correction for Human Parsing

An out-of-box human parsing representation extractor.

## TODO List

- [x] Inference code on three popular single person human parsing datasets.
- [x] Training code
- [ ] Extension on multi-person and video human parsing tasks.

Coming Soon! Stay tuned!

## Requirements

```
Python >= 3.5, Tensorflow == 1.14
```

## Trained models

The easiest way to get started is to use our trained SCHP models on your own images to extract human parsing representations. Here we provided trained models on three popular datasets. Theses three datasets have different label system, you can choose the best one to fit on your own task.

freeze model:  **SMEX** ([schp.pb](https://drive.google.com/file/d/1MILf5siCsoigrCuQaS_B5nRT1fn9hn9i/view?usp=sharing))

## Inference

To extract the human parsing representation, simply put your own image in the `Input_Directory`, download a pretrained model and run the following command. The output images with the same file name will be saved in `Output_Directory`

```
python check_frozen.py --dataset Dataset --restore_weight Checkpoint_Path --input Input_Directory --output Output_Directory
```

The `Dataset` command has three options, including 'smex'. Note each pixel in the output images denotes the predicted label number. The output images have the same size as the input ones. To better visualization, we put a palette with the output images. We suggest you to read the image with `PIL`.

## Training by Dockerfile
```text
    docker build -t auto_train --build-arg username=default --build-arg password=123456 .
    - username : username account gitlab project
    - password: password login into gitlab project
```
## Visualization

* Source Image.
![demo](./test/input/image_1578032868373.jpeg)

* smex Parsing Result.
![demo-smex](./test/output/image_1578032868373.png)

