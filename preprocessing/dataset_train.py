import numpy as np
import cv2.cv2 as cv2
import pandas as pd
import os

from config import cfg
from preprocessing.dataset import SMFDataset

pretrained_settings = {
    'resnet101': {
        'imagenet': {
            'input_space': 'BGR',
            'input_size': [224, 224, 3],
            'input_range': [0, 1],
            'mean': [0.406, 0.456, 0.485],
            'std': [0.225, 0.224, 0.229],
            'num_classes': 1000
        }
    },
}


def convert_data_to_pd():
    lst_images = os.listdir(cfg.path_labels)
    rows = []
    cols = ["Input_images", "Labels", "id"]
    for i in lst_images:
        l_temp = [os.path.join(cfg.path_images, i.replace('png', 'jpg')),
                  os.path.join(cfg.path_labels, i),
                  "0"]
        rows.append(l_temp)

    df = pd.DataFrame(data=rows, columns=cols)
    df.to_csv("dataset_train.csv", index=False)


def transform(inp):
    inp = inp / 255.
    inp = (inp - pretrained_settings['resnet101']['imagenet']['mean']) / pretrained_settings['resnet101']['imagenet'][
        'std']
    return inp


def transform_edge(edge):
    edge = cv2.cvtColor(edge, cv2.COLOR_BGR2GRAY)
    edge = edge / 255.
    edge = np.expand_dims(edge, axis=-1)
    return edge


def transform_body_pose(inp):
    colors = {
        'background': (0, 0, 0),
        'left_arm': (255, 215, 50),
        'right_arm': (85, 155, 45),
        'trunk': (170, 35, 170),
        'left_leg': (35, 70, 170),
        'right_leg': (100, 175, 165),
        'hair': (0, 0, 255),
        'face': (255, 0, 0)
    }

    keys = ["background", "hair", "face", "trunk", "left_arm", "right_arm", "left_leg", "right_leg"]
    lst_body_pose = []
    for k in keys:
        seg = cv2.inRange(inp, colors[k], colors[k])
        seg = seg / 255.
        seg = np.expand_dims(seg, axis=2)
        lst_body_pose.append(seg)
    return np.concatenate(lst_body_pose, axis=2)


def pre_processing(batchs):
    # initial
    smf = SMFDataset(input_size=cfg.data_shape)
    # images
    images = []
    # labels
    labels = []
    # labels edge
    edge_labels = []

    for index, row in batchs.iterrows():
        # Input_infer_parsing
        img = cv2.imread(row['Input_images'])
        img, _ = smf.process(img, transform=transform)

        # Labels
        lb = cv2.imread(row['Labels'])
        edge = np.copy(lb)
        lb, _ = smf.process(lb, transform=transform_body_pose)

        # Edge
        edge, _ = smf.process(edge, transform=transform_edge)

        images.append(img)
        labels.append(lb)
        edge_labels.append(edge)

    return np.array(images), np.array(labels), np.array(edge_labels)


if __name__ == '__main__':
    convert_data_to_pd()
    # data = pd.read_csv(cfg.path_train)
    # batch = 8
    # print('test')
    # for i in range(0, len(data), batch):
    #     batchs = data.iloc[i:i + batch]
    #
    #     images, labels, edge_lb = pre_processing(batchs)
    #     print(images.shape)
    #     print(labels.shape)
    #     print(edge_lb.shape)
