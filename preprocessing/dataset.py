import cv2.cv2 as cv2
import numpy as np

from preprocessing.dataset_func import get_affine_transform, transform_logits


class SMFDataset():

    def __init__(self, input_size=[512, 512]):
        # super(SMFDataset, self).__init__()
        self.input_size = input_size
        self.aspect_ratio = input_size[1] * 1.0 / input_size[0]
        self.input_size = np.asarray(input_size)

    def _box2cs(self, box):
        x, y, w, h = box[:4]
        return self._xywh2cs(x, y, w, h)

    def _xywh2cs(self, x, y, w, h):
        center = np.zeros((2), dtype=np.float32)
        center[0] = x + w * 0.5
        center[1] = y + h * 0.5
        if w > self.aspect_ratio * h:
            h = w * 1.0 / self.aspect_ratio
        elif w < self.aspect_ratio * h:
            w = h * self.aspect_ratio
        scale = np.array([w, h], dtype=np.float32)
        return center, scale

    def process(self, img, transform=None):
        h, w, _ = img.shape

        # Get person center and scale
        person_center, s = self._box2cs([0, 0, w - 1, h - 1])
        r = 0
        trans = get_affine_transform(person_center, s, r, self.input_size)
        input = cv2.warpAffine(
            img,
            trans,
            (int(self.input_size[1]), int(self.input_size[0])),
            flags=cv2.INTER_LINEAR,
            borderMode=cv2.BORDER_CONSTANT,
            borderValue=(0, 0, 0))
        if transform is not None:
            input = transform(input)
        meta = {
            'center': person_center,
            'height': h,
            'width': w,
            'scale': s,
            'rotation': r
        }

        return input, meta

    def restore_data_by_meta(self, img, meta):
        c = meta['center']
        h = meta['height']
        w = meta['width']
        s = meta['scale']
        transform_img = transform_logits(img, c, s, w, h, input_size=self.input_size)
        return transform_img


if __name__ == '__main__':

    img_path = "/media/duongpd/Data/dataset_SFX/data_viton/train/segmentss/000001_0.png"
    smex = SMFDataset(input_size=[512, 512])
    img = cv2.imread(img_path, cv2.IMREAD_COLOR)
    img, meta = smex.process(img)
    img_r = smex.restore_data_by_meta(img, meta)
    cv2.imshow("im", img)
    cv2.imshow('im_restore', img_r)
    cv2.waitKey(0)